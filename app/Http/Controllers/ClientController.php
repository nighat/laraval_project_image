<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Client;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *client
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //show Data
        //$clients = Client::all();
        //$clients =DB::table('client')->paginate(5);
        // $clients = Client::paginate(5);
        //return view('client.index',['clients'=>$clients]);
        $search = \Request::get('search');
        $clients = Client::where('clientname','like','%'.$search.'%')->orderBy('id')->paginate(6);
        return view('client.index',['clients' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $this->validate($request,[
            'clientname'=>'required',
            'organization'=>'required',
            'designation'=>'required',
            'address'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'auth'=>'required',
            'auth_mobile'=>'required',
            'reference'=>'required',
            'parpose'=>'required',
            'item'=>'required',
            'description'=>'required',
            'requirements'=>'required',
            'domain'=>'required',
            'orderdate'=>'required',
            'renewdate'=>'required',
            'status'=>'required',
            'advance'=>'required',
            'dues'=>'required',
            'cash'=>'required',
            'total'=>'required',
            'cheque'=>'required',
            'chequeno'=>'required',
            'bankname'=>'required',
            'brance'=>'required',
            'statusa'=>'required',


        ]);
        //create new data
        $client = new client;
        $client->clientname=$request->clientname;
        $client->organization=$request->organization;
        $client->designation=$request->designation;
        $client->address=$request->address;
        $client->mobile=$request->mobile;
        $client->email=$request->email;
        $client->auth=$request->auth;
        $client->auth_mobile=$request->auth_mobile;
        $client->reference=$request->reference;
        $client->parpose=$request->parpose;
        $client->item=$request->item;
        $client->description=$request->description;
        $client->requirements=$request->requirements;
        $client->domain=$request->domain;
        $client->orderdate=$request->orderdate;
        $client->renewdate=$request->renewdate;
        $client->status=$request->status;
        $client->advance=$request->advance;
        $client->dues=$request->dues;
        $client->cash=$request->cash;
        $client->total=$request->total;
        $client->cheque=$request->cheque;
        $client->chequeno=$request->chequeno;
        $client->bankname=$request->bankname;
        $client->brance=$request->brance;
        $client->statusa=$request->statusa;
        $client->save();
        return redirect()->route('client.index')->with('alert-success','Data Has Been Save!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);

        return view('client.show')->with('client',$client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
        return view('client.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        $this->validate($request,[
            'clientname'=>'required',
            'organization'=>'required',
            'designation'=>'required',
            'address'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'auth'=>'required',
            'auth_mobile'=>'required',
            'reference'=>'required',
            'parpose'=>'required',
            'item'=>'required',
            'description'=>'required',
            'requirements'=>'required',
            'domain'=>'required',
            'orderdate'=>'required',
            'renewdate'=>'required',
            'status'=>'required',
            'advance'=>'required',
            'dues'=>'required',
            'cash'=>'required',
            'total'=>'required',
            'cheque'=>'required',
            'chequeno'=>'required',
            'bankname'=>'required',
            'brance'=>'required',
            'statusa'=>'required',
        ]);

        $client = Client::findOrFail($id);
        $client->clientname=$request->clientname;
        $client->organization=$request->organization;
        $client->designation=$request->designation;
        $client->address=$request->address;
        $client->mobile=$request->mobile;
        $client->email=$request->email;
        $client->auth=$request->auth;
        $client->auth_mobile=$request->auth_mobile;
        $client->reference=$request->reference;
        $client->parpose=$request->parpose;
        $client->item=$request->item;
        $client->description=$request->description;
        $client->requirements=$request->requirements;
        $client->domain=$request->domain;
        $client->orderdate=$request->orderdate;
        $client->renewdate=$request->renewdate;
        $client->status=$request->status;
        $client->advance=$request->advance;
        $client->dues=$request->dues;
        $client->cash=$request->cash;
        $client->total=$request->total;
        $client->cheque=$request->cheque;
        $client->chequeno=$request->chequeno;
        $client->bankname=$request->bankname;
        $client->brance=$request->brance;
        $client->statusa=$request->statusa;
        $client->save();

        return redirect()->route('client.index')->with('alert-success','Data Hasbeen Saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //DELETE DATA
        $client = Client::findOrFail($id);
        $client->delete();
        return redirect()->route('client.index')->with('alert-success','Data Has Been Deleted!');

    }
}
