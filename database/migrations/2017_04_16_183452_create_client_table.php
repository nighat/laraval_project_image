<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clientname');
            $table->string('organization');
            $table->string('designation');
            $table->string('address');
            $table->float('mobile');
            $table->string('email');
            $table->string('auth');
            $table->float('auth_mobile');
            $table->string('reference');
            $table->string('parpose');
            $table->string('item');
            $table->string('description');
            $table->string('requirements');
            $table->string('domain');
            $table->date('orderdate');
            $table->date('renewdate');
            $table->string('status');
            $table->float('advance');
            $table->float('dues');
            $table->float('cash');
            $table->float('total');
            $table->string('cheque');
            $table->float('chequeno');
            $table->string('bankname');
            $table->string('brance');
            $table->string('statusa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
