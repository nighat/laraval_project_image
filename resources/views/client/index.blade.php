@extends('master')
@section('content')
    <div class="form-group row add">
        <div class="col-md-6">


        </div>

    </div>
    <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
            <input type="text" name="search" class="form-control" placeholder="search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <div class="row">
        <h2 style="text-align: center">Client Information</h2>
        <table class="table table-striped">
            <tr>
                <th> No.</th>
                <th> Client Name</th>
                <th> Organization</th>
                <th> Designation</th>
                <th> Address</th>
                <th> Mobile</th>
                <th> Email</th>
                <th> Auth</th>
                <th> Auth_mobile</th>
                <th> Reference</th>
                <th> Parpose</th>
                <th> Item</th>
                <th> Description</th>
                <th> Requirements</th>
                <th> Domain</th>
                <th> Order Date</th>
                <th> Renew Date</th>
                <th> Status</th>
                <th> Advance</th>
                <th> Dues</th>
                <th> Cash</th>
                <th> Total</th>
                <th> Cheque</th>
                <th> Chequeno</th>
                <th> Bankname</th>
                <th> Bank Brance</th>
                <th> Statusa</th>
                <th> Action</th>
            </tr>
            <a href="{{(route('client.create'))}}" class="btn btn-info pull-left">Create New Data</a><br>
            <?php $no=1;?>
            @foreach($clients as $client)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$client->clientname}}</td>
                    <td>{{$client->organization}}</td>
                    <td>{{$client->designation}}</td>
                    <td>{{$client->address}}</td>
                    <td>{{$client->mobile}}</td>
                    <td>{{$client->email}}</td>
                    <td>{{$client->auth}}</td>
                    <td>{{$client->auth_mobile}}</td>
                    <td>{{$client->reference}}</td>
                    <td>{{$client->parpose}}</td>
                    <td>{{$client->item}}</td>
                    <td>{{$client->description}}</td>
                    <td>{{$client->requirements}}</td>
                    <td>{{$client->domain}}</td>
                    <td>{{$client->orderdate}}</td>
                    <td>{{$client->renewdate}}</td>
                    <td>{{$client->status}}</td>
                    <td>{{$client->advance}}</td>
                    <td>{{$client->dues}}</td>
                    <td>{{$client->cash}}</td>
                    <td>{{$client->total}}</td>
                    <td>{{$client->cheque}}</td>
                    <td>{{$client->chequeno}}</td>
                    <td>{{$client->bankname}}</td>
                    <td>{{$client->brance}}</td>
                    <td>{{$client->statusa}}</td>

                    <td>
                        <form class="" action="{{route('client.destroy',$client->id)}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <a href="{{route('client.show',$client->id)}}" class="btn btn-primary">Show</a>
                            <a href="{{route('client.edit',$client->id)}}" class="btn btn-primary">Edit</a>
                            <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure to delete this data');" name="name" value="delete">



                        </form>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>
      {!! $clients->links() !!}
@stop

