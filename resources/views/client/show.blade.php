@extends('master')
@section('content')
    <div class="form-group row add">
        <div class="col-md-6">


        </div>

    </div>

    <div class="row">
        <h2 style="text-align: center">Single Client Information</h2>
        <table class="table table-striped">


            <th> Client Name</th>
            <th> Organization</th>
            <th> Designation</th>
            <th> Address</th>
            <th> Mobile</th>
            <th> Email</th>
            <th> Auth</th>
            <th> Auth_mobile</th>
            <th> Reference</th>
            <th> Parpose</th>
            <th> Item</th>
            <th> Description</th>
            <th> Requirements</th>
            <th> Domain</th>
            <th> Order Date</th>
            <th> Renew Date</th>
            <th> Status</th>
            <th> Advance</th>
            <th> Dues</th>
            <th> Cash</th>
            <th> Total</th>
            <th> Cheque</th>
            <th> Chequeno</th>
            <th> Bankname</th>
            <th> Bank Brance</th>
            <th> Statusa</th>



                <tr>

                    <td>{{$client->clientname}}</td>
                    <td>{{$client->organization}}</td>
                    <td>{{$client->designation}}</td>
                    <td>{{$client->address}}</td>
                    <td>{{$client->mobile}}</td>
                    <td>{{$client->email}}</td>
                    <td>{{$client->auth}}</td>
                    <td>{{$client->auth_mobile}}</td>
                    <td>{{$client->reference}}</td>
                    <td>{{$client->parpose}}</td>
                    <td>{{$client->item}}</td>
                    <td>{{$client->description}}</td>
                    <td>{{$client->requirements}}</td>
                    <td>{{$client->domain}}</td>
                    <td>{{$client->orderdate}}</td>
                    <td>{{$client->renewdate}}</td>
                    <td>{{$client->status}}</td>
                    <td>{{$client->advance}}</td>
                    <td>{{$client->dues}}</td>
                    <td>{{$client->cash}}</td>
                    <td>{{$client->total}}</td>
                    <td>{{$client->cheque}}</td>
                    <td>{{$client->chequeno}}</td>
                    <td>{{$client->bankname}}</td>
                    <td>{{$client->brance}}</td>
                    <td>{{$client->statusa}}</td>

                </tr>


        </table>
        <a href="{{(route('client.index'))}}" class="btn btn-info pull-left">Back To Client</a><br>
    </div>

@stop

