@extends('master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 style="text-align: center">Edit Data</h3>
        </div>
    </div>
    <div class="row">
        <form class="" action="{{route('client.update',$client->id)}}" method="post">
            <input name="_method" type="hidden" value="PATCH">
            {{csrf_field()}}
            <table  >


                <tr style="text-align: center"><td style="padding-left:350px " >
                        <p>Client Name:</p>
                        <div class="form-group{{($errors->Has('clientname'))?$errors->first('clientname'):''}}">
                            <input type="text" name="clientname" class="form-control" placeholder="Enter clientname here" value="{{$client->clientname}}">
                            {!! $errors->first('clientname','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px " >
                        <p>Organization</p>
                        <div class="form-group{{($errors->Has('organization'))?$errors->first('organization'):''}}">
                            <input type="text" name="organization" class="form-control" placeholder="Enter organization here" value="{{$client->organization}}">
                            {!! $errors->first('organization','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>Designation:</p>
                        <div class="form-group{{($errors->Has('designation'))?$errors->first('designation'):''}}">
                            <input type="text" name="designation" class="form-control" placeholder="Enter Designation here" value="{{$client->designation}}">
                            {!! $errors->first('designation','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>

                <tr style="text-align: center"><td style="padding-left:350px " >
                        <p>Address</p>
                        <div class="form-group{{($errors->Has('address'))?$errors->first('address'):''}}">
                            <input type="text" name="address" class="form-control" placeholder="Enter address here" value="{{$client->address}}">
                            {!! $errors->first('address','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>mobile Number</p>
                        <div class="form-group{{($errors->Has('mobile'))?$errors->first('mobile'):''}}">
                            <input type="number" name="mobile" class="form-control" placeholder="Enter mobile here" value="{{$client->mobile}}">
                            {!! $errors->first('mobile','<p class="help-block">:message</p>') !!}
                        </div>

                    </td>

                    <td style="padding-left:30px " >
                        <p>E-mail </p>
                        <div class="form-group{{($errors->Has('email'))?$errors->first('email'):''}}">
                            <input type="text" name="email" class="form-control" placeholder="Enter email date here" value="{{$client->email}}">
                            {!! $errors->first('email','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>


                <tr style="text-align: center"><td style="padding-left:350px " >

                        <p>Auth Person</p>
                        <div class="form-group{{($errors->Has('auth'))?$errors->first('auth'):''}}">
                            <input type="text" name="auth" class="form-control" placeholder="Enter auth person here" value="{{$client->auth}}">
                            {!! $errors->first('auth','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>


                    <td style="padding-left:30px "> <p>Auth Mobile</p>
                        <div class="form-group{{($errors->Has('auth_mobile'))?$errors->first('auth_mobile'):''}}">
                            <input type="number" name="auth_mobile" class="form-control" placeholder="Enter auth_mobile here" value="{{$client->auth_mobile}}">
                            {!! $errors->first('auth_mobile','<p class="help-block">:message</p>') !!}
                        </div>

                    </td><td style="padding-left:30px "> <p>Reference</p>
                        <div class="form-group{{($errors->Has('reference'))?$errors->first('reference'):''}}">
                            <input type="text" name="reference" class="form-control" placeholder="Enter reference here" value="{{$client->reference}}">
                            {!! $errors->first('reference','<p class="help-block">:message</p>') !!}
                        </div>

                    </td>
                </tr>


                <tr style="text-align: center"><td style="padding-left:350px " >

                        <p>parpose</p>
                        <div class="form-group{{($errors->Has('parpose'))?$errors->first('parpose'):''}}">
                            <input type="text" name="parpose" class="form-control" placeholder="Enter parpose here" value="{{$client->parpose}}">
                            {!! $errors->first('parpose','<p class="help-block">:message</p>') !!}
                        </div>
                        <h3> Information: <hr></h3>
                    </td>

                </tr>


                <tr style="text-align: center"><td style="padding-left:350px " >
                        <p>Item</p>
                        <div class="form-group{{($errors->Has('item'))?$errors->first('item'):''}}">
                            <input type="text" name="item" class="form-control" placeholder="Enter item here" value="{{$client->item}}">
                            {!! $errors->first('item','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>Description</p>
                        <div class="form-group{{($errors->Has('description'))?$errors->first('description'):''}}">
                            <input type="text" name="description" class="form-control" placeholder="Enter description here" value="{{$client->description}}">
                            {!! $errors->first('description','<p class="help-block">:message</p>') !!}
                        </div>

                    </td>

                    <td style="padding-left:30px " >
                        <p>Ex-requirements </p>
                        <div class="form-group{{($errors->Has('requirements'))?$errors->first('requirements'):''}}">
                            <input type="text" name="requirements" class="form-control" placeholder="Enter requirements here" value="{{$client->requirements}}">
                            {!! $errors->first('requirements','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr style="text-align: center"><td style="padding-left:350px " >
                        <p>Domain Name</p>
                        <div class="form-group{{($errors->Has('domain'))?$errors->first('domain'):''}}">
                            <input type="text" name="domain" class="form-control" placeholder="Enter domain here" value="{{$client->domain}}">
                            {!! $errors->first('domain','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>Order Date</p>
                        <div class="form-group{{($errors->Has('orderdate'))?$errors->first('orderdate'):''}}">
                            <input type="date" name="orderdate" class="form-control" placeholder="Enter order date here" value="{{$client->orderdate}}">
                            {!! $errors->first('orderdate','<p class="help-block">:message</p>') !!}
                        </div>

                    </td>

                    <td style="padding-left:30px " >
                        <p>Renew Date </p>
                        <div class="form-group{{($errors->Has('renewdate'))?$errors->first('renewdate'):''}}">
                            <input type="date" name="renewdate" class="form-control" placeholder="Enter renewdate here" value="{{$client->renewdate}}">
                            {!! $errors->first('renewdate','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>
                <tr style="text-align: center"><td style="padding-left:350px " >

                        <p>Status</p>
                        <div class="form-group{{($errors->Has('status'))?$errors->first('status'):''}}">
                            <input type="text" name="status" class="form-control" placeholder="Enter status here" value="{{$client->status}}">
                            {!! $errors->first('status','<p class="help-block">:message</p>') !!}
                        </div>
                        <h3> Pay-method:<hr></h3>
                    </td>

                </tr>

                <tr style="text-align: center">


                    <td style="padding-left:350px ">
                        <p>Advance</p>
                        <div class="form-group{{($errors->Has('advance'))?$errors->first('advance'):''}}">
                            <input type="number" name="advance" class="form-control" placeholder="Enter advance here" value="{{$client->advance}}">
                            {!! $errors->first('advance','<p class="help-block">:message</p>') !!}
                        </div>

                    </td>

                    <td style="padding-left:30px " >
                        <p>Dues </p>
                        <div class="form-group{{($errors->Has('dues'))?$errors->first('dues'):''}}">
                            <input type="number" name="dues" class="form-control" placeholder="Enter dues here" value="{{$client->dues}}">
                            {!! $errors->first('dues','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px " >
                        <p>Cash</p>
                        <div class="form-group{{($errors->Has('cash'))?$errors->first('cash'):''}}">
                            <input type="number" name="cash" class="form-control" placeholder="Enter cash here" value="{{$client->cash}}">
                            {!! $errors->first('cash','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>

                </tr>
                <tr style="text-align: center">
                    <td style="padding-left:350px " >
                        <p>Total Amount</p>
                        <div class="form-group{{($errors->Has('total'))?$errors->first('total'):''}}">
                            <input type="number" name="total" class="form-control" placeholder="Enter total here" value="{{$client->total}}" >
                            {!! $errors->first('total','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px " >
                        <p>Cheque</p>
                        <div class="form-group{{($errors->Has('cheque'))?$errors->first('cheque'):''}}">
                            <input type="text" name="cheque" class="form-control" placeholder="Enter cheque here" value="{{$client->cheque}}">
                            {!! $errors->first('cheque','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px " >
                        <p>Cheque Number</p>
                        <div class="form-group{{($errors->Has('chequeno'))?$errors->first('chequeno'):''}}">
                            <input type="number" name="chequeno" class="form-control" placeholder="Enter cheque here" value="{{$client->chequeno}}">
                            {!! $errors->first('chequeno','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>



                </tr>
                <tr style="text-align: center"><td style="padding-left:350px " >

                        <p>Bank name</p>
                        <div class="form-group{{($errors->Has('bankname'))?$errors->first('bankname'):''}}">
                            <input type="text" name="bankname" class="form-control" placeholder="Enter bankname here" value="{{$client->bankname}}">
                            {!! $errors->first('bankname','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px " >
                        <p>Brance Name</p>
                        <div class="form-group{{($errors->Has('brance'))?$errors->first('brance'):''}}">
                            <input type="text" name="brance" class="form-control" placeholder="Enter brance here" value="{{$client->brance}}">
                            {!! $errors->first('brance','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>Status</p>
                        <div class="form-group{{($errors->Has('statusa'))?$errors->first('statusa'):''}}">
                            <input type="text" name="statusa" class="form-control" placeholder="Enter status here" value="{{$client->statusa}}">
                            {!! $errors->first('statusa','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>

                </tr>
                <tr style="text-align: center"><td style="padding-left:350px " >

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="save">
                            <a href="{{(route('client.index'))}}" class="btn btn-info pull-left">Return</a>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
@stop