@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
                <div class="panel-body">
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="blog">Blog</a></li>
                            <li class="active"><a href="client">Client</a></li>
                            <li class="active"><a href="newclient">Appointment</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#"></a></li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
