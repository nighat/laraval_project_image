@extends('master')
@section('content')
    <div class="form-group row add">
        <div class="col-md-6">

        </div>

    </div>
    <div class="row">
        <h2 style="text-align: center">Single Blog Information</h2>
        <table class="table table-striped">


                <th style="text-align: center"> Title</th>
                <th style="text-align: center" > Description</th>

                <tr>
                    <td style="text-align: center">{{$blog->title}}</td>
                    <td style="text-align: center">{{$blog->description}}</td>
                </tr>

        </table>
        <a href="{{(route('blog.index'))}}" class="btn btn-info pull-left">Back To File</a><br>
    </div>

@stop
