@extends('master')
@section('content')
    <h2>Add new post</h2>


    <form class="" action="/blog" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend> Form</legend>
        <input type="text" name="title" value="" placeholder="this is title"><br>
        {{ ($errors->has('title')) ? $errors->first('title') : '' }} <br>
        <textarea name="description" rows="3" cols="22" placeholder="this is description"></textarea><br>
        {{ ($errors->has('description')) ? $errors->first('description') : '' }} <br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <img src="http://placehold.it/100x100" id="showimages" style="max-width:200px;max-height:200px;float:left;"/>
        <div class="row">
            <div class="col-md-12">
                <input type="file" id="inputimages" name="images">
            </div>
        </div>
            <br>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary pull-left" value="post">
                Submit
            </button>
            <a href="{{(route('blog.index'))}}" class="btn btn-success pull-left">Return</a><br>
        </div>
        </fieldset>
    </form>


@stop