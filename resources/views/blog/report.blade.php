@extends('master')
@section('content')
    <div class="form-group row add">
        <div class="col-md-6">

        </div>

    </div>
    <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
            <input type="text" name="search" class="form-control" placeholder="search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <div class="row">
        <h3 style="text-align: center">Blog Information</h3>
        <table class="table table-striped">
            <tr>
                <th> No.</th>
                <th> Title</th>
                <th> Description</th>
                <th> Action</th>
            </tr>
            <a href="{{(route('blog.create'))}}" class="btn btn-info pull-left">Create New Data</a><br>
            <?php $no=1;?>
            @foreach($blogs as $blog)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$blog->title}}</td>
                    <td>{{$blog->description}}</td>
                    <td>
                        <form class="" action="{{route('blog.destroy',$blog->id)}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <a href="{{route('blog.show',$blog->id)}}" class="btn btn-primary">Show</a>
                            <a href="{{route('blog.edit',$blog->id)}}" class="btn btn-primary">Edit</a>

                            <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure to delete this data');" name="name" value="delete">



                        </form>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>
    {!! $blogs->links() !!}
@stop

<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/21/2017
 * Time: 12:31 AM
 */