@extends('master')
@section('content')
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>
    <div class="row">


        <h3 style="text-align: center">Edit Data</h3>
        <form class="" action="{{route('blog.update',$blog->id)}}" method="post">
            <input name="_method" type="hidden" value="PATCH">
            {{csrf_field()}}
            <h4>Title</h4>
            <div class="form-group{{ ($errors->has('title')) ? $errors->first('title') : '' }}">
                <input type="text" name="title" class="form-control" placeholder="Enter Title Here" value="{{$blog->title}}">
                {!! $errors->first('title','<p class="help-block">:message</p>') !!}
            </div>
            <h4>Description</h4>
            <div class="form-group{{ ($errors->has('description')) ? $errors->first('title') : '' }}">
                <input type="text" name="description" class="form-control" placeholder="Enter Description Here" value="{{$blog->description}}">
                {!! $errors->first('description','<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="save">
                <a href="{{(route('blog.index'))}}" class="btn btn-info pull-left">Return</a><br>

            </div>
        </form>
    </div>
@stop

